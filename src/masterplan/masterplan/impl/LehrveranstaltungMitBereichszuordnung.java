package masterplan.impl;

import masterplan.Bereich;

public abstract class LehrveranstaltungMitBereichszuordnung extends LehrveranstaltungBase {
    private Bereich bereich;

    public LehrveranstaltungMitBereichszuordnung(int creditPoints, String title, String type, Bereich bereich) throws IllegalArgumentException {
        super(creditPoints, title, String.format("%s im Bereich %s", type, bereich.getDescription()));
        this.bereich = bereich;
    }

    /**
     * @return the bereich
     */
    public Bereich getBereich() {
        return bereich;
    }
}
