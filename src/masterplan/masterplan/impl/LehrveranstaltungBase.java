package masterplan.impl;

import masterplan.Lehrveranstaltung;

public abstract class LehrveranstaltungBase implements Lehrveranstaltung {
    private int creditPoints;
    private String description;

    public LehrveranstaltungBase(int creditPoints, String... args) throws IllegalArgumentException {
        this.creditPoints = creditPoints;
        if (args.length == 1) {
            this.description = args[0];
        } else if (args.length == 2) {
            this.description = String.format("%s (%s)", args[0], args[1]);
        } else {
            throw new IllegalArgumentException("To many arguments");
        }
    }

    /**
     * @return Die Anzahl der Credit Points, welche diese lehrveranstaltung
     *         einbringt.
     */
    public int getCreditPoints() {
        return this.creditPoints;
    }

    /**
     * @return Eine Beschreibung dieser lehrveranstaltung.
     */
    public String getDescription() {
        return this.description;
    }
}
