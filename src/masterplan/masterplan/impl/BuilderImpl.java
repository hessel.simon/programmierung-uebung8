package masterplan.impl;

import masterplan.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BuilderImpl implements MasterplanBuilder, SemesterBuilder {
    private Lehrveranstaltung[][] semesters = new Lehrveranstaltung[10][10];
    private int currentSemester = 0;
    private int currentLehrveranstaltung = 0;

    public BuilderImpl() {
    }

    public Masterplan validateAndCreate() throws InvalidMasterplanException {
        Lehrveranstaltung[][] tovalidate = shrinkArray();
        HashMap<Bereich, Integer> creditPointsBereich = new HashMap<Bereich, Integer>();
        HashMap<String, Integer> creditPointsLehrveranstaltung = new HashMap<String, Integer>();
        int totalCreditPoints = 0;

        for (Lehrveranstaltung[] semester : tovalidate) {
            for (Lehrveranstaltung lehrveranstaltung : semester) {

                int creditPoints = lehrveranstaltung.getCreditPoints();

                String name = lehrveranstaltung.getClass().getSimpleName();
                switch (name) {
                case "Wahlpflichtvorlesung":
                case "Seminar":
                    Bereich bereich = ((LehrveranstaltungMitBereichszuordnung) lehrveranstaltung).getBereich();
                    creditPointsBereich.merge(bereich, creditPoints, Integer::sum);
                    totalCreditPoints += creditPoints;

                    if (name.equals("Seminar")) {
                        if (!creditPointsLehrveranstaltung.containsKey(name)) {
                            creditPointsLehrveranstaltung.put(name, creditPoints);
                        } else {
                            throw new InvalidMasterplanException(String.format("%s can't be selected twice.", name));
                        }
                    }

                    break;
                case "Anwendungsfach":
                    creditPointsLehrveranstaltung.merge(name, creditPoints, Integer::sum);
                    totalCreditPoints += creditPoints;
                    break;
                case "Masterarbeit":
                case "Schwerpunktkolloquium":
                case "Praktikum":
                    if (!creditPointsLehrveranstaltung.containsKey(name)) {
                        creditPointsLehrveranstaltung.put(name, creditPoints);
                        totalCreditPoints += creditPoints;
                    } else {
                        throw new InvalidMasterplanException(String.format("%s can't be selected twice.", name));
                    }
                    break;
                }
            }
        }

        if (!creditPointsLehrveranstaltung.containsKey("Anwendungsfach") && creditPointsLehrveranstaltung.size() < 4
                || creditPointsLehrveranstaltung.size() < 3) {
            throw new InvalidMasterplanException("Masterarbeit, Schwerpunktkolloquium or Praktikum wasn't selected.");
        }
        if (creditPointsBereich.size() < 3) {
            throw new InvalidMasterplanException("At least 3 different scopes have to be picked.");
        }
        if (creditPointsLehrveranstaltung.containsKey("Anwendungsfach")
                && creditPointsLehrveranstaltung.get("Anwendungsfach") < 18) {
            throw new InvalidMasterplanException(
                    "Exactly 18 credit points must have been earned for the application subject.");
        }
        if (!creditPointsBereich.containsKey(Bereich.THEORETISCHE_INFORMATIK)) {
            throw new InvalidMasterplanException(
                    "At least 12 Credit Points from the field of Theoretical Computer Science must have been earned.");
        }

        for (Map.Entry<Bereich, Integer> entry : creditPointsBereich.entrySet()) {
            Bereich bereich = entry.getKey();
            int creditPoints = entry.getValue();
            if (creditPoints > 35) {
                throw new InvalidMasterplanException(String.format(
                        "A maximum of 35 credit points may be earned in the subject %s.", bereich.getDescription()));
            } else if (bereich == Bereich.THEORETISCHE_INFORMATIK && creditPoints < 12) {
                throw new InvalidMasterplanException(
                        "At least 12 Credit Points from the field of Theoretical Computer Science must have been earned.");
            }
        }

        if (totalCreditPoints < 120) {
            throw new InvalidMasterplanException("At least 120 points must be scored.");
        }

        return new MasterplanImpl(tovalidate);

    }

    private Lehrveranstaltung[][] shrinkArray() {
        int semesters = 0;
        HashMap<Integer, Integer> veranstaltungen = new HashMap<Integer, Integer>();

        for (int i = 0; i < this.semesters.length; i++) {
            if (this.semesters[i][0] == null) {
                semesters = i;
                break;
            }
            for (int j = 0; j < this.semesters[i].length; j++) {
                if (this.semesters[i][j] == null) {
                    veranstaltungen.put(i, j);
                    break;
                }
            }

        }

        Lehrveranstaltung[][] newarray = new Lehrveranstaltung[semesters][];
        for (Map.Entry<Integer, Integer> entry : veranstaltungen.entrySet()) {
            int semester = entry.getKey();
            int veranstaltung = entry.getValue();
            newarray[semester] = Arrays.copyOf(this.semesters[semester], veranstaltung);

        }

        return newarray;

    }

    public SemesterBuilder beginSemester() throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
        }
        return this;
    }

    public MasterplanBuilder endSemester() {
        this.currentSemester++;
        this.currentLehrveranstaltung = 0;
        return this;
    }

    public boolean checkSpaceInSemester() throws InvalidMasterplanException {
        if (this.currentLehrveranstaltung >= 0 && this.currentLehrveranstaltung < 10) {
            return true;
        } else {
            throw new InvalidMasterplanException("the currentSemester does excied the limits.");
        }
    }

    public SemesterBuilder anwendungsfach(int creditPoints, String title) throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Anwendungsfach(creditPoints,
                    title);
            this.currentLehrveranstaltung++;
            return this;
        }
        return null;

    }

    public SemesterBuilder masterarbeit(String title) throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Masterarbeit(title);
            this.currentLehrveranstaltung++;
            return this;
        }
        return null;
    }

    public SemesterBuilder praktikum(String title) throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Praktikum(title);
            this.currentLehrveranstaltung++;
            return this;

        }
        return null;
    }

    public SemesterBuilder schwerpunktkolloquium(String title) throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Schwerpunktkolloquium(title);
            this.currentLehrveranstaltung++;
            return this;
        }
        return null;
    }

    public SemesterBuilder seminar(String title, Bereich bereich) throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Seminar(title, bereich);
            this.currentLehrveranstaltung++;
            return this;
        }
        return null;
    }

    public SemesterBuilder wahlpflichtvorlesung(int creditPoints, String title, Bereich bereich)
            throws InvalidMasterplanException {
        if (this.checkSpaceInSemester()) {
            this.semesters[this.currentSemester][this.currentLehrveranstaltung] = new Wahlpflichtvorlesung(creditPoints,
                    title, bereich);
            this.currentLehrveranstaltung++;
            return this;
        }
        return null;
    }

}
