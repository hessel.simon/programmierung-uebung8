package masterplan.impl;

import masterplan.Masterplan;
import masterplan.Lehrveranstaltung;

public class MasterplanImpl implements Masterplan {
    private Lehrveranstaltung[][] lehrveranstaltungen;

    public MasterplanImpl(Lehrveranstaltung[][] lehrveranstaltungen) {
        this.lehrveranstaltungen = lehrveranstaltungen;
    }

    public int getNumberOfSemesters() {
        return this.lehrveranstaltungen.length;
    }

    public int getNumberOfLehrveranstaltungen(int semster) {
        try {
            return this.lehrveranstaltungen[semster].length;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public Lehrveranstaltung getLehrveranstaltung(int semster, int position) {
        try {
            return this.lehrveranstaltungen[semster][position];
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

    }
}
