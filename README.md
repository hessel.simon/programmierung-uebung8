# Programmierung Übung 8 Aufgabe 3

## Folder structure

```
.
├── launcher
│   ├── launcher
│   │   └── Launcher.java
│   └── module-info.java
├── masterplan
│   ├── masterplan
│   │   ├── impl
│   │   │   ├── Anwendungsfach.java
│   │   │   ├── BuilderImpl.java
│   │   │   ├── LehrveranstaltungBase.java
│   │   │   ├── LehrveranstaltungMitBereichszuordnung.java
│   │   │   ├── Masterarbeit.java
│   │   │   ├── MasterplanImpl.java
│   │   │   ├── Praktikum.java
│   │   │   ├── Schwerpunktkolloquium.java
│   │   │   ├── Seminar.java
│   │   │   └── Wahlpflichtvorlesung.java
│   │   ├── test
│   │   │   └── MasterplanTest.java
│   │   ├── Bereich.java
│   │   ├── InvalidMasterplanException.java
│   │   ├── Lehrveranstaltung.java
│   │   ├── Masterplan.java
│   │   ├── MasterplanBuilder.java
│   │   └── SemesterBuilder.java
│   └── module-info.java
├── compile-tests.sh
├── compile.sh
├── hamcrest-core-1.3.jar
├── junit-4.13-rc-2.jar
├── run-tests.sh
└── run.sh
```
